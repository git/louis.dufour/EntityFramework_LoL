﻿using API.Dto;
using API.Mapping;
using EFlib;
using EFManager;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;
using System.Xml.Linq;


namespace API.Controllers.version1
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:ApiVersion}/[controller]")]
    public class ChampionController : ControllerBase
    {

        private readonly ManagerData data;
        // private readonly StubData data;
        private readonly ILogger<ChampionController> _logger;


        public ChampionController(ManagerData manager, ILogger<ChampionController> logger)
        {
            data = manager;
            _logger = logger;
        }

        /**** Méthodes GET ****/
        [HttpGet]
        public async Task<ActionResult<ChampionDto>> GetChamps()
        {
            // Récupération de la liste des champions
            IEnumerable<Champion?> Champs = await data.ChampionsMgr.GetItems(0, data.ChampionsMgr.GetNbItems().Result);
            if (Champs == null)
            {
                _logger.LogWarning("No chamions found"); 
                return NotFound();
            }

            // Création de la liste de champion Dto
            List<ChampionDto> DtoChamps = new List<ChampionDto>();

            // Chargement de la liste des champions Dto à partir des champions
            Champs.ToList().ForEach(Champ => DtoChamps.Add(Champ.ToDto()));

            return Ok(DtoChamps);
        }



        [HttpGet("{name}")]
        public async Task<ActionResult<ChampionDto>> GetChampByName(string name)
        {
            try
            {
                // Récupération de la liste des champions
                IEnumerable<Champion?> champion = await data.ChampionsMgr.GetItemsByName(name, 0, data.ChampionsMgr.GetNbItems().Result);

                // Enregistrement des log
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(GetChampByName), name);

                // Création du champion Dto
                ChampionDto resultat = champion.First().ToDto();

                // Vérification de sa véraciter 
                if (resultat == null)
                {
                    _logger.LogWarning("No chamions found with {name}", name); 
                    return NotFound();
                }
                return Ok(resultat);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        /**** Méthodes POST ****/
        [HttpPost("Ajouter")]
        public async Task<ActionResult> PostChamp([FromBody] ChampionDto championDto)
        {
            try
            {
                // Convertie le championDto en model (a était ajouté via l'API)
                Champion champion = championDto.ToModel();

                // Ajout du champion en BD
                await data.ChampionsMgr.AddItem(champion);

                _logger.LogInformation("Sucessfully saved Champions  : " + champion.Name);

                return CreatedAtAction(nameof(data.ChampionsMgr.GetItemsByName), new { championDto.Name }, championDto);
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);
            }
        }

        /**** Méthodes DELETE ****/

        [HttpDelete("Supprimer/{name}")]
        public async Task<IActionResult> DeleteChamp(string name)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(DeleteChamp), name);
                var champion = await data.ChampionsMgr.GetItemsByName(name, 0, await data.ChampionsMgr.GetNbItems());

                if (champion != null) await data.ChampionsMgr.DeleteItem(champion.First());
                else
                {
                    _logger.LogError($"No chamions found with {name} cannot delete"); ;
                    return NotFound($"No chamions found with {name} cannot delete");
                }
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);
            }
        }


        /**** Méthodes PUT ****/

        [HttpPut("Modifier/{name}")]
        public async Task<ActionResult> PutChampName(string name, [FromBody] ChampionDto value)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with name : {championName}", nameof(PutChampName), name);
                var champion = await data.ChampionsMgr.GetItemsByName(name, 0, await data.ChampionsMgr.GetNbItems());
                if (champion == null)
                {
                    _logger.LogError("No chamions found with {name} in the dataBase", name); ;
                    return NotFound();
                }

                await data.ChampionsMgr.UpdateItem(champion.First(), value.ToModel());
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);
            }
        }
    }
}
