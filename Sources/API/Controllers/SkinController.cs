﻿using API.Dto;
using API.Mapping;
using EFManager;
using Microsoft.AspNetCore.Mvc;
using Model;
using StubLib;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SkinController : ControllerBase
    {

        private readonly ManagerData data;
        // private readonly StubData data;

        private readonly ILogger<SkinController> _logger;

        public SkinController(ManagerData manager, ILogger<SkinController> logger)
        {
            data = manager;
            _logger = logger;
        }

        /**** Méthodes GET ****/
        [HttpGet]
        public async Task<ActionResult<SkinDto>> GetSkins()
        {
            // Récupération de la liste des skins
            IEnumerable<Skin?> Skins = await data.SkinsMgr.GetItems(0, data.SkinsMgr.GetNbItems().Result);
            if (Skins == null)
            {
                _logger.LogWarning("No skins found");
                return NotFound();
            }

            // Création de la liste de skin Dto
            List<SkinDto> DtoSkins = new List<SkinDto>();

            // Chargement de la liste des champions Dto à partir des champions
            Skins.ToList().ForEach(Skin => DtoSkins.Add(Skin.ToDto()));

            return Ok(DtoSkins);
        }

        [HttpGet("{name}")]
        public async Task<ActionResult<ChampionDto>> GetSkinByName(string name)
        {
            try
            {
                // Récupération de la liste des champions
                IEnumerable<Skin?> Skin = await data.SkinsMgr.GetItemsByName(name, 0, data.SkinsMgr.GetNbItems().Result);

                // Enregistrement des log
                _logger.LogInformation("Executing {Action} with name : {skinName}", nameof(GetSkinByName), name);

                // Création du champion Dto
                SkinDto resultat = Skin.First().ToDto();

                // Vérification de sa véraciter 
                if (resultat == null)
                {
                    _logger.LogWarning("No skins found with {name}", name);
                    return NotFound();
                }
                return Ok(resultat);

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        /**** Méthodes POST ****/
        [HttpPost("Ajouter")]
        public async Task<ActionResult> PostSkin([FromBody] SkinDto skinDto)
        {
            try
            {
                // Convertie le championDto en model (a était ajouté via l'API)
                Skin skin = skinDto.ToModel();

                // Ajout du champion en BD
                await data.SkinsMgr.AddItem(skin);

                _logger.LogInformation("Sucessfully saved Skins  : " + skin.Name);

                return CreatedAtAction(nameof(data.SkinsMgr.GetItemsByName), new { skinDto.Name }, skinDto);
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Skins controller : " + e.Message);
                return BadRequest(e.Message);
            }
        }

        /**** Méthodes DELETE ****/

        [HttpDelete("Supprimer/{name}")]
        public async Task<IActionResult> DeleteSkin(string name)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with name : {skinName}", nameof(DeleteSkin), name);
                var skin = await data.SkinsMgr.GetItemsByName(name, 0, await data.SkinsMgr.GetNbItems());

                if (skin != null) await data.SkinsMgr.DeleteItem(skin.First());
                else
                {
                    _logger.LogError($"No skins found with {name} cannot delete"); ;
                    return NotFound($"No skins found with {name} cannot delete");
                }
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Champions controller : " + e.Message);
                return BadRequest(e.Message);
            }
        }


        /**** Méthodes PUT ****/

        [HttpPut("Modifier/{name}")]
        public async Task<ActionResult> PutSkinName(string name, [FromBody] SkinDto value)
        {
            try
            {
                _logger.LogInformation("Executing {Action} with name : {skinName}", nameof(PutSkinName), name);
                var skin = await data.SkinsMgr.GetItemsByName(name, 0, await data.SkinsMgr.GetNbItems());
                if (skin == null)
                {
                    _logger.LogError("No skins found with {name} in the dataBase", name); ;
                    return NotFound();
                }

                await data.SkinsMgr.UpdateItem(skin.First(), value.ToModel());
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError("Somthing goes wrong caching the Skins controller : " + e.Message);
                return BadRequest(e.Message);
            }
        }
    }
}
