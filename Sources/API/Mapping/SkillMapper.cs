﻿using API.Dto;
using Model;

namespace API.Mapping
{
    public static class SkillMapper
    {
        public static SkillDto ToDto(this Skill skill)
        {
            if (skill == null)
            {
                throw new ArgumentNullException("Skill null");
            }

            return new SkillDto()
            {
                Name = skill.Name,
                Description = skill.Description,
                Type = skill.Type
            };
        }

        public static Skill toModel(this SkillDto skillDto)
        {
            if (skillDto == null)
            {
                throw new ArgumentNullException("DtoSkill null");
            }

            return new Skill(skillDto.Name, skillDto.Type, skillDto.Description);
        }
    }
}
