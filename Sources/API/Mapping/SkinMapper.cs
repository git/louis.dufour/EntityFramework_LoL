﻿using API.Dto;
using Model;

namespace API.Mapping
{
    public static class SkinMapper
    {

        public static SkinDto ToDto(this Skin skin)
        {
            if (skin == null)
            {
                throw new ArgumentNullException("Skin null");
            }

            return new SkinDto()
            {
                Name = skin.Name,
                ChampionName = skin.Champion.Name,
                Description = skin.Description,
                Price = skin.Price,
                Icon = skin.Icon,
                Image = skin.Image
            };
        }

        public static Skin ToModel(this SkinDto skinDto)
        {
            if (skinDto == null)
            {
                throw new ArgumentNullException("DtoSkin null");
            }
            return new Skin(skinDto.Name, new Champion(skinDto.Name), skinDto.Price, skinDto.Icon, skinDto.Image.Base64, skinDto.Description);
        }
    }
}
