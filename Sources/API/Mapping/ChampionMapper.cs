﻿using API.Dto;
using Model;
using System.Collections.ObjectModel;
using System.Collections.Immutable;

namespace API.Mapping
{
    public static class ChampionMapper
    {
        public static ChampionDto ToDto(this Champion champion)
        {
            if (champion == null)
            {
                throw new ArgumentNullException("Champion null");
            }

            return new ChampionDto
            {
                Name = champion.Name,
                Bio = champion.Bio,
                Icon = champion.Icon,

                NameCharac = champion.Characteristics.Keys,
                ValueCharac = champion.Characteristics.Values,

                Class = champion.Class,
                Skins = champion.Skins.Select(skin => skin.ToDto()),
                Skills = champion.Skills.Select(skill => skill.ToDto()),
                Image = champion.Image
            };
        }
        public static Champion ToModel(this ChampionDto DtoChamp)
        {
            if (DtoChamp == null)
            {
                throw new ArgumentNullException("DtoChampion null");
            }
            var champion = new Champion(DtoChamp.Name, DtoChamp.Class, DtoChamp.Icon, DtoChamp.Image.Base64, DtoChamp.Bio);
            
            if (DtoChamp.Skills != null) foreach (var skill in DtoChamp.Skills) { champion.AddSkill(skill.toModel()); }
           
            var characteristics = DtoChamp.NameCharac.Zip(DtoChamp.ValueCharac, (name, value) => Tuple.Create(name, value));
            foreach (var characteristic in characteristics)
            {
                champion.AddCharacteristics(characteristic);
            }

            return champion;
        }
    }
}
