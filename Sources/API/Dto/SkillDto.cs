﻿using Model;

namespace API.Dto
{
    public class SkillDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public SkillType Type { get; set; }
    }
}
