﻿using Model;

namespace API.Dto
{
    public class SkinDto
    {
        public string Name { get; set; }
        public string ChampionName { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public string Icon { get; set; }
        public LargeImage Image { get; set; }
    }
}
