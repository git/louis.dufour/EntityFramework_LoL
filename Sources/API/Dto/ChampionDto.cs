﻿using EFlib;
using Model;
using System.Collections.Immutable;
using System.Collections.ObjectModel;

namespace API.Dto
{
    public class ChampionDto
    {
        /**** Only Attributs ****/
        public string Name { get; set; }
        public string Bio { get; set; }
        public string Icon { get; set; }
        
        // Obliger de split un dictionnaire pour le Json
        public IEnumerable<string> NameCharac { get; set; }
        public IEnumerable<int> ValueCharac { get; set; }
        
        public ChampionClass Class { get; set; }
        public IEnumerable<SkinDto> Skins { get; set; }
        public IEnumerable<SkillDto> Skills { get; set; }
        public LargeImage Image { get; set; }

    }
}
