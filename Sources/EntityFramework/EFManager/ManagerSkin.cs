﻿using EFlib;
using EFMapping;
using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFManager
{
    public partial class ManagerData
    {
        public class ManagerSkin : ISkinsManager
        {
            private readonly ManagerData parent;
            public ManagerSkin(ManagerData parent)
                => this.parent = parent;
            public async Task<Skin?> AddItem(Skin? item)
            {
                try
                {
                    await parent.DbContext.Skins.AddAsync(item.ToEF(parent.DbContext));
                    parent.DbContext.SaveChangesAsync();
                }
                catch (OperationCanceledException) { }
                catch (DbUpdateException) { }
                return item;
            }

            public async Task<bool> DeleteItem(Skin? item)
            {
                try
                {
                    var toDelete = parent.DbContext.Skins.Find(item.Name);
                    if (toDelete != null)
                    {
                        parent.DbContext.Skins.Remove(toDelete);
                        parent.DbContext.SaveChangesAsync();
                        return true;
                    }
                    return false;
                }
                catch (DbUpdateException)
                {
                    return false;
                }
            }

            public async Task<Skin?> UpdateItem(Skin? oldItem, Skin? newItem)
            {
                var toUpdate = parent.DbContext.Skins.Find(oldItem.Name);
                try
                {
                    toUpdate.Champion = parent.DbContext.Champions.Find(newItem.Champion.Name);
                    parent.DbContext.SaveChanges();
                }
                catch (DbUpdateException) { }
                return newItem;

            }

            public async Task<IEnumerable<Skin?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                if (orderingPropertyName != null)
                {
                    if (descending)
                    {
                        return await Task.FromResult(parent.DbContext.Skins.OrderByDescending(skin => typeof(EFSkin).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(efSkin => efSkin.ToModel()));
                    }
                    else
                    {
                        return await Task.FromResult(parent.DbContext.Skins.OrderBy(skin => typeof(EFSkin).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(efSkin => efSkin.ToModel()));
                    }
                }
                else
                {
                    return await Task.FromResult(parent.DbContext.Skins.Skip(index * count).Take(count).Select(efSkin => efSkin.ToModel()));
                }
            }

            public async Task<int> GetNbItems()
            {
                return parent.DbContext.Skins.Count();
            }

            public Task<IEnumerable<Skin?>> GetItemsByChampion(Champion? champion, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Skin?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByChampion(Champion? champion)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByName(string substring)
            {
                throw new NotImplementedException();
            }
        }
    }
}
