﻿using Model;
using EFlib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EFManager
{
    public partial class ManagerData : IDataManager
    {
        protected SQLiteContext DbContext { get; }
        
        public IChampionsManager ChampionsMgr { get; }

        public ISkinsManager SkinsMgr { get; }

        public IRunesManager RunesMgr { get; }

        public IRunePagesManager RunePagesMgr { get; }
   
        public ManagerData(SQLiteContext ContextData)
        {
            DbContext = ContextData;
            ChampionsMgr = new ManagerChampion(this);
            SkinsMgr = new ManagerSkin(this);
        }
    }
}
