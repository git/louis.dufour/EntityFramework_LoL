﻿using EFlib;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Model;
using EFMapping;

namespace EFManager
{
    public partial class ManagerData
    { 
        public class ManagerChampion : IChampionsManager
        {
            private readonly ManagerData parent;
            public ManagerChampion(ManagerData parent)
                => this.parent = parent;

            public async Task<Champion?> AddItem(Champion? item)
            {
                try
                {
                    await parent.DbContext.Champions.AddAsync(item.ToEF(parent.DbContext));
                    parent.DbContext.SaveChangesAsync();
                }
                catch (OperationCanceledException){}
                catch(DbUpdateException){}
                return item;
            }

            public async Task<bool> DeleteItem(Champion? item)
            {
                try
                {
                    var toDelete = parent.DbContext.Champions.Find(item.Name);
                    if (toDelete != null)
                    {
                        parent.DbContext.Champions.Remove(toDelete);
                        parent.DbContext.SaveChangesAsync();
                        return true;
                    }
                    return false;
                }
                catch (DbUpdateException)
                {
                    return false;
                }
            }

            public async Task<Champion?> UpdateItem(Champion? oldItem, Champion? newItem)
            {
                var toUpdate = parent.DbContext.Champions.Find(oldItem.Name);
                try
                {
                    toUpdate = newItem.ToEF(parent.DbContext);
                    parent.DbContext.SaveChanges();
                }
                catch(DbUpdateException) { }
                return newItem;
            }

            public async Task<IEnumerable<Champion?>> GetItems(int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                if (orderingPropertyName != null)
                {
                    if (descending)
                    {
                        return await Task.FromResult(parent.DbContext.Champions.OrderByDescending(champ => typeof(EFChampion).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(efChampion => efChampion.ToModel()));
                    }
                    else
                    {
                        return await Task.FromResult(parent.DbContext.Champions.OrderBy(champ => typeof(EFChampion).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(efChampion => efChampion.ToModel()));
                    }
                }
                else
                {
                    return await Task.FromResult(parent.DbContext.Champions.Skip(index * count).Take(count).Select(efChampion => efChampion.ToModel()));
                }
            }

            public async Task<int> GetNbItems()
            {
                return parent.DbContext.Champions.Count();
            }

            public async Task<IEnumerable<Champion?>> GetItemsByName(string substring, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {                
                var EfChampion = from champ in parent.DbContext.Champions where champ.Name.Contains(substring) select champ;
                if (orderingPropertyName != null)
                {
                    if (descending)
                    {
                        return await Task.FromResult(EfChampion.OrderByDescending(champ => typeof(EFChampion).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(efChampion => efChampion.ToModel()));
                    }
                    else
                    {
                        return await Task.FromResult(EfChampion.OrderBy(champ => typeof(EFChampion).GetProperty(orderingPropertyName)).Skip(index * count).Take(count).Select(efChampion => efChampion.ToModel()));
                        
                    }
                }
                else
                {
                   return await Task.FromResult(EfChampion.Skip(index * count).Take(count).Select(efChampion => efChampion.ToModel()));
                }
            }

            public Task<IEnumerable<Champion?>> GetItemsByCharacteristic(string charName, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();

            }

            public Task<IEnumerable<Champion?>> GetItemsByClass(ChampionClass championClass, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();

            }


            public Task<IEnumerable<Champion?>> GetItemsByRunePage(RunePage? runePage, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();

            }

            public Task<IEnumerable<Champion?>> GetItemsBySkill(Skill? skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }

            public Task<IEnumerable<Champion?>> GetItemsBySkill(string skill, int index, int count, string? orderingPropertyName = null, bool descending = false)
            {
                throw new NotImplementedException();
            }
            public async Task<int> GetNbItemsByCharacteristic(string charName)
            {
                throw new NotImplementedException();
            }

            public async Task<int> GetNbItemsByClass(ChampionClass championClass)
            {
                throw new NotImplementedException();
            }

            public async Task<int> GetNbItemsByName(string substring)
            {
                throw new NotImplementedException();
            }

            public Task<int> GetNbItemsByRunePage(RunePage? runePage)
            {
                throw new NotImplementedException();
            }

            public async Task<int> GetNbItemsBySkill(Skill? skill)
            {
                throw new NotImplementedException();
            }

            public async Task<int> GetNbItemsBySkill(string skill)
            {
                throw new NotImplementedException();
            }
        }
    }
}