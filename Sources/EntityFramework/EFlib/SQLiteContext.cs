﻿using Microsoft.EntityFrameworkCore;

namespace EFlib
{
    public class SQLiteContext : DbContext
    {
        /**** Attributs ****/
        public DbSet<EFChampion> Champions { get; set; }
        public DbSet<EFCharacteristics> Characteristics { get; set; }
        public DbSet<EFLargeImage> LargeImages { get; set; }
        public DbSet<EFSkin> Skins { get; set; }
        public DbSet<EFSkill> Skills { get; set; }

        /**** Méthodes ****/
        public SQLiteContext()
        { }

        public SQLiteContext(DbContextOptions<SQLiteContext> options)
            : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite($"Data Source=projet.dbloulou.db");
            }
        }
    }
}
