﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFlib
{
    public class EFCharacteristics
    {
        [Key]
        [MaxLength(250)]
        public string Name { get; set; }
        [Required]
        public int Value { get; set; }
        [Required]
        public string NameChampion { get; set; }
        [ForeignKey("NameChampion")]
        public EFChampion Champion { get; set; }
    }
}
