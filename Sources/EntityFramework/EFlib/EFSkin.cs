﻿using Model;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace EFlib
{
    public class EFSkin
    {
        /**** Attributs ****/
        [Key]
        public string Name { get; set; }

        public string Description { get; set; }
        public string Icon { get; set; }
        public float Price { get; set; }

        public Guid ImageId { get; set; }
        [ForeignKey("ImageId")]
        public EFLargeImage Image { get; set; }

        public string NameChampion { get; set; }
        [ForeignKey("NameChampion")]
        public EFChampion Champion { get; set; }

    }
}
