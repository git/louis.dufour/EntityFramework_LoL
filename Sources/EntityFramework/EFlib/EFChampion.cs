﻿
using Model;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFlib
{
    public class EFChampion
    {
        /**** Only Attributs ****/
        [Key]
        [MaxLength(250)]
        public string Name { get; set; }
        
        [MaxLength(500)]
        public string Bio { get; set; }
        public string Icon { get; set; }

        // Propriété de navigation pour les paires clé-valeur
        public virtual ICollection<EFCharacteristics> Characteristics { get; set; }

        [Required]
        public ChampionClass Class { get; set; }
        public ReadOnlyCollection<EFSkin>? Skins { get; set; }
        public virtual ICollection<EFSkill> Skills { get; set; }

        public Guid ImageId { get; set; }
        [ForeignKey("ImageId")]
        public EFLargeImage Image { get; set; }
    }
}