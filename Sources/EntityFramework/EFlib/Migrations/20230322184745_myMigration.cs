﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFlib.Migrations
{
    /// <inheritdoc />
    public partial class myMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LargeImages",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Base64 = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LargeImages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 250, nullable: false),
                    Description = table.Column<string>(type: "TEXT", maxLength: 500, nullable: false),
                    Type = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "Champions",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 250, nullable: false),
                    Bio = table.Column<string>(type: "TEXT", maxLength: 500, nullable: false),
                    Icon = table.Column<string>(type: "TEXT", nullable: false),
                    Class = table.Column<int>(type: "INTEGER", nullable: false),
                    ImageId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Champions", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Champions_LargeImages_ImageId",
                        column: x => x.ImageId,
                        principalTable: "LargeImages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Characteristics",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", maxLength: 250, nullable: false),
                    Value = table.Column<int>(type: "INTEGER", nullable: false),
                    NameChampion = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characteristics", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Characteristics_Champions_NameChampion",
                        column: x => x.NameChampion,
                        principalTable: "Champions",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EFChampionEFSkill",
                columns: table => new
                {
                    ChampionsName = table.Column<string>(type: "TEXT", nullable: false),
                    SkillsName = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFChampionEFSkill", x => new { x.ChampionsName, x.SkillsName });
                    table.ForeignKey(
                        name: "FK_EFChampionEFSkill_Champions_ChampionsName",
                        column: x => x.ChampionsName,
                        principalTable: "Champions",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EFChampionEFSkill_Skills_SkillsName",
                        column: x => x.SkillsName,
                        principalTable: "Skills",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Skins",
                columns: table => new
                {
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: false),
                    Icon = table.Column<string>(type: "TEXT", nullable: false),
                    Price = table.Column<float>(type: "REAL", nullable: false),
                    ImageId = table.Column<Guid>(type: "TEXT", nullable: false),
                    NameChampion = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skins", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Skins_Champions_NameChampion",
                        column: x => x.NameChampion,
                        principalTable: "Champions",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Skins_LargeImages_ImageId",
                        column: x => x.ImageId,
                        principalTable: "LargeImages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Champions_ImageId",
                table: "Champions",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_Characteristics_NameChampion",
                table: "Characteristics",
                column: "NameChampion");

            migrationBuilder.CreateIndex(
                name: "IX_EFChampionEFSkill_SkillsName",
                table: "EFChampionEFSkill",
                column: "SkillsName");

            migrationBuilder.CreateIndex(
                name: "IX_Skins_ImageId",
                table: "Skins",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_Skins_NameChampion",
                table: "Skins",
                column: "NameChampion");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Characteristics");

            migrationBuilder.DropTable(
                name: "EFChampionEFSkill");

            migrationBuilder.DropTable(
                name: "Skins");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "Champions");

            migrationBuilder.DropTable(
                name: "LargeImages");
        }
    }
}
