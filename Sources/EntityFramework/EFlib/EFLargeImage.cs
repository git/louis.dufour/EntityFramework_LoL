﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFlib
{
    public class EFLargeImage
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Base64 { get; set; }
    }
}
