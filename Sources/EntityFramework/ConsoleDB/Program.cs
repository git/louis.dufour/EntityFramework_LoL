﻿using EFlib;

//StubData stub = new StubData();
//stub.ChampionsMgr.GetItems(0, 5);


static class Program
{
    private static void Main(string[] args)
    {
        EFChampion boss1 = new EFChampion { Name = "bigBoss", Bio = "KingOfMetal", Icon = "vide" };
        EFChampion boss2 = new EFChampion { Name = "Soon", Bio = "Indomptable", Icon = "vide" };
        EFChampion boss3 = new EFChampion { Name = "doctorWho", Bio = "Le silence", Icon = "vide" };

        // Clear la base
        using (var context = new SQLiteContext())
        {
            foreach (var n in context.Champions)
            {
                context.Remove(n);
                Console.WriteLine($"La base est clear");
            }
            context.SaveChanges();
        }

        // Create
        using (var context = new SQLiteContext())
        {
            // Crée des EFChampion et les insère dans la base
            Console.WriteLine("Creates and inserts new EFChampion");
            context.Add(boss1);
            context.Add(boss2);
            context.Add(boss3);
            context.SaveChanges();

            foreach (var n in context.Champions)
            {
                Console.WriteLine($"{n.Name}");
            }
        }

        // Delete
        using (var context = new SQLiteContext())
        {
            foreach (var n in context.Champions)
            {
                if (n.Name == "Soon")
                {
                    Console.WriteLine($"Viens d'être supprimer - {n.Name}");
                    context.Remove(n);
                }
            }
            context.SaveChanges();
        }

        // Update
        using (var context = new SQLiteContext())
        {
            foreach (var n in context.Champions)
            {
                if (n.Name == "bigBoss")
                {
                    n.Name = "miniBoss";
                    Console.WriteLine($"Viens d'être changer - {n.Name}");
                }
            }
            context.SaveChanges();
        }
    }
}