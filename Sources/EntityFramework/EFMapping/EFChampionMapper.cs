﻿using EFlib;
using Model;
using System.Collections.Immutable;
using System.Reflection.PortableExecutable;

namespace EFMapping
{
    public static class EFChampionMapper
    {

        public static EFChampion ToEF(this Champion Champ, SQLiteContext context)
        {

            EFChampion? EfChampion = context.Champions.Find(Champ.Name);
            if (EfChampion == null)
            {
                EfChampion = new()
                {
                    Name = Champ.Name,
                    Bio = Champ.Bio,
                    Icon = Champ.Icon,
                    Class = Champ.Class,
                    Image = new() { Id = Guid.NewGuid(), Base64 = Champ.Image.Base64 },


                };
                EfChampion.Skills = Champ.Skills.Select(skill => skill.ToEF(EfChampion, context)).ToList();
                EfChampion.Characteristics = Champ.Characteristics.Select(Charac => Charac.ToEF(EfChampion, context)).ToList();
            }
            return EfChampion;

        }

        public static Champion ToModel(this EFChampion EFChamp)
        {
            var champion = new Champion(EFChamp.Name, EFChamp.Class, EFChamp.Icon, EFChamp.Image.ToModel().Base64, EFChamp.Bio);
            if (EFChamp.Skills != null) foreach (var skill in EFChamp.Skills) { champion.AddSkill(skill.ToModel()); }
            if (EFChamp.Characteristics != null) foreach (var charac in EFChamp.Characteristics) { champion.AddCharacteristics(charac.ToModel()); }
            return champion;
        }
    }
}