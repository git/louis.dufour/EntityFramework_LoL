﻿using EFlib;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFMapping
{
    public static class EFLargeImageMapper
    {
        public static EFLargeImage ToEF(this LargeImage LargeImage, SQLiteContext context)
        {
            var EfLargeImage = context.LargeImages.Find(LargeImage.Base64);
            if (EfLargeImage == null)
            {
                return new()
                {
                    Id = Guid.NewGuid(),
                    Base64 = LargeImage.Base64
                };
            }
            return EfLargeImage;
        }
        public static LargeImage ToModel(this EFLargeImage EFlargeImage) => new LargeImage(EFlargeImage.Base64);
    }
}
