﻿using EFlib;
using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EFMapping
{
    public static class EFCharacteristicsMapper
    {
        public static EFCharacteristics ToEF(this KeyValuePair<string, int> item, EFChampion champion, SQLiteContext context)
        {
            var EfCharacteristics = context.Characteristics.FirstOrDefault(c => c.Name == item.Key && c.Champion.Name == champion.Name);

            if (EfCharacteristics == null)
            {
                return new()
                {
                    Name = item.Key,
                    Value = item.Value,
                    NameChampion = champion.Name
                };
            }
            return EfCharacteristics;
        }

        public static Tuple<string, int> ToModel(this EFCharacteristics Charac)=> new(Charac.Name, Charac.Value);

    }
}
