﻿using EFlib;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFMapping
{
    public static class EFSkillMapper
    {
        public static EFSkill ToEF(this Skill skill, EFChampion champ, SQLiteContext context)
        {
            var EfSkill = context.Skills.Find(skill.Name);
            if (EfSkill == null)
            {
                return new()
                {
                    Name = skill.Name,
                    Description = skill.Description,
                    Type = skill.Type,
                    Champions = new List<EFChampion>() { champ }
                };
            }
            EfSkill!.Champions?.Add(champ);
            return EfSkill;
        }
        public static Skill ToModel(this EFSkill skill)=> new(skill.Name, skill.Type, skill.Description);
    }
}
