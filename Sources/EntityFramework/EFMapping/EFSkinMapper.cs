﻿using EFlib;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFMapping
{
    public static class EFSkinMapper
    {
        public static EFSkin ToEF(this Skin skin, SQLiteContext context)
        {
            var EfSkin = context.Skins.Find(skin.Name);
            if (EfSkin == null)
            {
                return new()
                {
                    Name = skin.Name,
                    Description = skin.Description,
                    Icon = skin.Icon,
                    Price = skin.Price,
                    Image = new() { Id = Guid.NewGuid(), Base64 = skin.Image.Base64 },
                    NameChampion = skin.Champion.Name,
                    Champion = context?.Champions.Find(skin.Champion.Name) ?? skin.Champion.ToEF(context)
                };
            }
            return EfSkin;
        }

        public static Skin ToModel(this EFSkin Skin)=> new(Skin.Name, Skin.Champion.ToModel(), Skin.Price, Skin.Icon, Skin.Image.Base64, Skin.Description);
    }
}
