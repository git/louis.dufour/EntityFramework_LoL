﻿using EFlib;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEF
{
    public class UnitTestSkin
    {
        [Fact]
        public async Task AddSkin_Test()
        {
            // connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>().UseSqlite(connection).Options;

            using (var context = new SQLiteContext(options))
            {
                await context.Database.EnsureCreatedAsync();

                // Arrange
                var champion = new EFChampion
                {
                    Name = "Champion 1",
                    Bio = "Bio Champion 1",
                    Icon = "aatrox_icon.jpg",
                    Class = ChampionClass.Assassin,
                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "CheminDeMonChampion" }
                };

                var skin = new EFSkin
                {
                    Name = "Mecha Kingdoms Aatrox",
                    Description = "In a distant cyberfuture, the champion Aatrox becomes a symbol of ultimate power and glory...",
                    Icon = "aatrox_mecha_icon.jpg",
                    Price = 1350,
                    NameChampion = "Champion 1",
                    ImageId = Guid.NewGuid(),
                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "aatrox_mecha_splash.jpg" }
                };

                var skinsList = new List<EFSkin>();
                skinsList.Add(skin);
                champion.Skins = new ReadOnlyCollection<EFSkin>(skinsList);

                context.Add(champion);
                await context.SaveChangesAsync();

                // Act
                var championFromDb = await context.Champions.Include(c => c.Skins).FirstOrDefaultAsync(c => c.Name == "Champion 1");

                // Assert
                Assert.NotNull(championFromDb);
                Assert.Single(championFromDb.Skins);
                if (championFromDb.Skins.Any())
                {
                    Assert.Equal("Mecha Kingdoms Aatrox", championFromDb.Skins[0].Name);
                }
            }
        }

        [Fact]
        public async Task UpdateSkin_Test()
        {
            // connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>().UseSqlite(connection).Options;

            using (var context = new SQLiteContext(options))
            {
                await context.Database.EnsureCreatedAsync(); // pour créer la base si elle n'existe pas déjà

                // Arrange
                var champion = new EFChampion
                {
                    Name = "Champion 1",
                    Bio = "Bio Champion 1",
                    Icon = "aatrox_icon.jpg",
                    Class = ChampionClass.Assassin,

                    Characteristics = new List<EFCharacteristics>
            {
                new EFCharacteristics { Name = "Attack Damage", Value = 60 },
                new EFCharacteristics { Name = "Armor", Value = 38 },
                new EFCharacteristics { Name = "Magic Resist", Value = 32 }
            },

                    Skills = ImmutableHashSet.Create<EFSkill>(
                        new EFSkill { Name = "Skill 1", Description = "Desc Skill 1" },
                        new EFSkill { Name = "Skill 2", Description = "Desc Skill 2" }
                    ),

                    Skins = new ReadOnlyCollection<EFSkin>(new List<EFSkin>
            {
                new EFSkin
                {
                    Name = "Mecha Kingdoms Aatrox",
                    Description = "In a distant cyberfuture, the champion Aatrox becomes a symbol of ultimate power and glory...",
                    Icon = "aatrox_mecha_icon.jpg",
                    Price = 1350,
                    NameChampion = "Aatrox",
                    ImageId = Guid.NewGuid(),
                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "aatrox_mecha_splash.jpg" }
                }
            }),

                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "CheminDeMonChampion" }
                };

                context.Add(champion);
                await context.SaveChangesAsync();

                var skinToUpdate = champion.Skins.FirstOrDefault();

                skinToUpdate.Description = "New description"; // update the description

                // Act
                context.Update(skinToUpdate);
                await context.SaveChangesAsync();

                // Assert
                var updatedSkin = await context.Skins.FirstOrDefaultAsync(s => s.Name == skinToUpdate.Name);
                Assert.NotNull(updatedSkin);
                Assert.Equal("New description", updatedSkin.Description);
            }
        }

        // Pour le test de delete on est obligé de supprimer le champion à cause de la clef étrangère, car on part du principe qu'un skin ne peut exister s'il y a un champion
        [Fact]
        public async Task DeleteSkin_Test()
        {
            // connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>().UseSqlite(connection).Options;

            using (var context = new SQLiteContext(options))
            {
                await context.Database.EnsureCreatedAsync();

                // Arrange
                EFChampion champion = new EFChampion
                {
                    Name = "Champion 1",
                    Bio = "Bio Champion 1",
                    Icon = "aatrox_icon.jpg",
                    Class = ChampionClass.Assassin,

                    Characteristics = new List<EFCharacteristics> {
                new EFCharacteristics { Name = "Attack Damage", Value = 60 },
                new EFCharacteristics { Name = "Armor", Value = 38 },
                new EFCharacteristics { Name = "Magic Resist", Value = 32 }
            },

                    Skills = ImmutableHashSet.Create<EFSkill>(
                        new EFSkill { Name = "Skill 1", Description = "Desc Skill 1" },
                        new EFSkill { Name = "Skill 2", Description = "Desc Skill 2" }
                    ),

                    Skins = new ReadOnlyCollection<EFSkin>(new List<EFSkin> {
                new EFSkin
                {
                    Name = "Mecha Kingdoms Aatrox",
                    Description = "In a distant cyberfuture, the champion Aatrox becomes a symbol of ultimate power and glory...",
                    Icon = "aatrox_mecha_icon.jpg",
                    Price = 1350,
                    NameChampion = "Aatrox",
                    ImageId = Guid.NewGuid(),
                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "aatrox_mecha_splash.jpg" }
                } }),

                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "CheminDeMonChampion" }
                };

                context.Add(champion);
                await context.SaveChangesAsync();

                // Act
                context.Champions.Remove(champion);
                await context.SaveChangesAsync();

                // Assert
                var deletedChampion = await context.Champions.FirstOrDefaultAsync(c => c.Name == "Champion 1");
                Assert.Null(deletedChampion);

                var deletedSkin = await context.Skins.FirstOrDefaultAsync(s => s.Name == "Mecha Kingdoms Aatrox");
                Assert.Null(deletedSkin);
            }
        }

        [Fact]
        public async Task GetSkin_Test()
        {
            // connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>().UseSqlite(connection).Options;

            using (var context = new SQLiteContext(options))
            {
                await context.Database.EnsureCreatedAsync();

                // Arrange
                var champion = new EFChampion
                {
                    Name = "Champion 1",
                    Bio = "Bio Champion 1",
                    Icon = "aatrox_icon.jpg",
                    Class = ChampionClass.Assassin,
                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "CheminDeMonChampion" }
                };

                var skin = new EFSkin
                {
                    Name = "Mecha Kingdoms Aatrox",
                    Description = "In a distant cyberfuture, the champion Aatrox becomes a symbol of ultimate power and glory...",
                    Icon = "aatrox_mecha_icon.jpg",
                    Price = 1350,
                    NameChampion = "Champion 1",
                    ImageId = Guid.NewGuid(),
                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "aatrox_mecha_splash.jpg" }
                };

                champion.Skins = new ReadOnlyCollection<EFSkin>(new List<EFSkin> { skin });
                context.Add(champion);
                await context.SaveChangesAsync();

                // Act
                var skinFromDb = await context.Skins.Include(s => s.Champion).FirstOrDefaultAsync(s => s.Name == "Mecha Kingdoms Aatrox");

                // Assert
                Assert.NotNull(skinFromDb);
                Assert.Equal("Mecha Kingdoms Aatrox", skinFromDb.Name);
                Assert.Equal("Champion 1", skinFromDb.Champion.Name);
            }
        }



    }
}
