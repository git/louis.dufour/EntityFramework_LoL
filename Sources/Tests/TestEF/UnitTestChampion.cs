using EFlib;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model;
using StubEF;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Reflection.PortableExecutable;
using Xunit;

namespace TestEF
{
    public class UniTestChampion
    {
        [Fact]
        public async Task AddChampion_Test()
        {
            // connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource =:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>().UseSqlite(connection).Options;

            using (var context = new SQLiteContext(options))
            {
                await context.Database.EnsureCreatedAsync(); //pour cr�er la base si elle n'existe pas d�j�         

                // Arrange
                EFChampion champion = new EFChampion
                {
                    Name = "Champion 1",
                    Bio = "Bio Champion 1",
                    Icon = "aatrox_icon.jpg",
                    Class = ChampionClass.Assassin,

                    Characteristics = new List<EFCharacteristics> {
                            new EFCharacteristics { Name = "Attack Damage", Value = 60 },
                            new EFCharacteristics { Name = "Armor", Value = 38 },
                            new EFCharacteristics { Name = "Magic Resist", Value = 32 }
                    },

                    Skills = ImmutableHashSet.Create<EFSkill>(
                        new EFSkill { Name = "Skill 1", Description = "Desc Skill 1" },
                        new EFSkill { Name = "Skill 2", Description = "Desc Skill 2" }
                    ),

                    Skins = new ReadOnlyCollection<EFSkin>(new List<EFSkin> {
                    new EFSkin
                    {
                        Name = "Mecha Kingdoms Aatrox",
                        Description = "In a distant cyberfuture, the champion Aatrox becomes a symbol of ultimate power and glory...",
                        Icon = "aatrox_mecha_icon.jpg",
                        Price = 1350,
                        NameChampion = "Aatrox",
                        ImageId = Guid.NewGuid(),
                        Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "aatrox_mecha_splash.jpg" }
                    } }),

                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "CheminDeMonChampion" }
                };

                context.Add(champion);
                await context.SaveChangesAsync();

                // Act
                await context.Champions.FirstOrDefaultAsync(c => c.Name == "Champion 1");

                // Assert
                Assert.NotNull(champion);
                Assert.Equal("Bio Champion 1", champion.Bio);
                Assert.Equal("aatrox_icon.jpg", champion.Icon);
                Assert.NotNull(champion.Characteristics);
                Assert.Equal(3, champion.Characteristics.Count);
                Assert.Equal(ChampionClass.Assassin, champion.Class);
                Assert.Single(champion.Skins);
                if (champion.Skins.Any())
                {
                    Assert.Equal("Mecha Kingdoms Aatrox", champion.Skins[0].Name);
                }
                Assert.Equal(2, champion.Skills.Count);
                Assert.Contains(champion.Skills, s => s.Name == "Skill 1");
                Assert.Contains(champion.Skills, s => s.Name == "Skill 2");
                Assert.Equal("CheminDeMonChampion", champion.Image.Base64);
            }
        }

        [Fact]
        public async Task UpdateChampion_Test()
        {
            // connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource =:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>().UseSqlite(connection).Options;

            using (var context = new SQLiteContext(options))
            {
                await context.Database.EnsureCreatedAsync(); // pour cr�er la base si elle n'existe pas d�j�         

                // Arrange
                var champion = new EFChampion
                {
                    Name = "Champion 1",
                    Bio = "Bio Champion 1",
                    Icon = "aatrox_icon.jpg",
                    Class = ChampionClass.Assassin,

                    Characteristics = new List<EFCharacteristics> {
                new EFCharacteristics { Name = "Attack Damage", Value = 60 },
                new EFCharacteristics { Name = "Armor", Value = 38 },
                new EFCharacteristics { Name = "Magic Resist", Value = 32 }
            },

                    Skills = ImmutableHashSet.Create<EFSkill>(
                        new EFSkill { Name = "Skill 1", Description = "Desc Skill 1" },
                        new EFSkill { Name = "Skill 2", Description = "Desc Skill 2" }
                    ),

                    Skins = new ReadOnlyCollection<EFSkin>(new List<EFSkin> {
                new EFSkin
                {
                    Name = "Mecha Kingdoms Aatrox",
                    Description = "In a distant cyberfuture, the champion Aatrox becomes a symbol of ultimate power and glory...",
                    Icon = "aatrox_mecha_icon.jpg",
                    Price = 1350,
                    NameChampion = "Aatrox",
                    ImageId = Guid.NewGuid(),
                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "aatrox_mecha_splash.jpg" }
                } }),

                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "CheminDeMonChampion" }
                };

                context.Add(champion);
                await context.SaveChangesAsync();

                champion.Bio = "New Bio"; // Updating the bio of the champion

                // Act
                await context.SaveChangesAsync();

                var updatedChampion = await context.Champions.FirstOrDefaultAsync(c => c.Name == "Champion 1");

                // Assert
                Assert.NotNull(updatedChampion);
                Assert.Equal("New Bio", updatedChampion.Bio);
            }
        }

        [Fact]
        public async Task DeleteChampion_Test()
        {
            // connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource =:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>().UseSqlite(connection).Options;

            using (var context = new SQLiteContext(options))
            {
                await context.Database.EnsureCreatedAsync(); //pour cr�er la base si elle n'existe pas d�j�         

                // Arrange
                var champion = new EFChampion
                {
                    Name = "ChampionToDelete",
                    Bio = "Bio Champion to delete",
                    Icon = "delete_icon.jpg",
                    Class = ChampionClass.Fighter,

                    Characteristics = new List<EFCharacteristics> {
                    new EFCharacteristics { Name = "Attack Damage", Value = 70 },
                    new EFCharacteristics { Name = "Armor", Value = 42 },
                    new EFCharacteristics { Name = "Magic Resist", Value = 28 }
            },

                    Skills = ImmutableHashSet.Create<EFSkill>(
                            new EFSkill { Name = "Skill 1", Description = "Desc Skill 1" }
                    ),

                    Skins = new ReadOnlyCollection<EFSkin>(new List<EFSkin> {
                    new EFSkin
                    {
                        Name = "Delete Skin",
                        Description = "Delete skin description",
                        Icon = "delete_skin_icon.jpg",
                        Price = 975,
                        NameChampion = "ChampionToDelete",
                        ImageId = Guid.NewGuid(),
                        Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "delete_skin_splash.jpg" }
                    }
            }),

                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "CheminDeMonChampionToDelete" }
                };

                context.Add(champion);
                await context.SaveChangesAsync();

                // Act
                context.Remove(champion);
                await context.SaveChangesAsync();

                // Assert
                var deletedChampion = await context.Champions.FirstOrDefaultAsync(c => c.Name == "ChampionToDelete");
                Assert.Null(deletedChampion);
            }
        }

        [Fact]
        public async Task GetChampion_Test()
        {
            // connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource =:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<SQLiteContext>().UseSqlite(connection).Options;

            using (var context = new SQLiteContext(options))
            {
                await context.Database.EnsureCreatedAsync(); // pour cr�er la base si elle n'existe pas d�j�

                // Arrange
                var championToAdd = new EFChampion
                {
                    Name = "ChampionToGet",
                    Bio = "Bio Champion to get",
                    Icon = "get_icon.jpg",
                    Class = ChampionClass.Marksman,

                    Characteristics = new List<EFCharacteristics> {
                new EFCharacteristics { Name = "Attack Damage", Value = 80 },
                new EFCharacteristics { Name = "Armor", Value = 32 },
                new EFCharacteristics { Name = "Magic Resist", Value = 24 }
            },

                    Skills = ImmutableHashSet.Create<EFSkill>(
                        new EFSkill { Name = "Skill 1", Description = "Desc Skill 1" },
                        new EFSkill { Name = "Skill 2", Description = "Desc Skill 2" }
                    ),

                    Skins = new ReadOnlyCollection<EFSkin>(new List<EFSkin> {
                new EFSkin
                {
                    Name = "Get Skin",
                    Description = "Get skin description",
                    Icon = "get_skin_icon.jpg",
                    Price = 520,
                    NameChampion = "ChampionToGet",
                    ImageId = Guid.NewGuid(),
                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "get_skin_splash.jpg" }
                }
            }),

                    Image = new EFLargeImage { Id = Guid.NewGuid(), Base64 = "CheminDeMonChampionToGet" }
                };

                context.Add(championToAdd);
                await context.SaveChangesAsync();

                // Act
                var championToGet = await context.Champions.Include(c => c.Characteristics)
                    .Include(c => c.Skills).Include(c => c.Skins).FirstOrDefaultAsync(c => c.Name == "ChampionToGet");

                // Assert
                Assert.NotNull(championToGet);
                Assert.Equal("Bio Champion to get", championToGet.Bio);
                Assert.Equal("get_icon.jpg", championToGet.Icon);
                Assert.NotNull(championToGet.Characteristics);
                Assert.Equal(3, championToGet.Characteristics.Count);
                Assert.Equal(ChampionClass.Marksman, championToGet.Class);
                Assert.Single(championToGet.Skins);
                if (championToGet.Skins.Any())
                {
                    Assert.Equal("Get Skin", championToGet.Skins[0].Name);
                }
                Assert.Equal(2, championToGet.Skills.Count);
                Assert.Contains(championToGet.Skills, s => s.Name == "Skill 1");
                Assert.Contains(championToGet.Skills, s => s.Name == "Skill 2");
                Assert.Equal("CheminDeMonChampionToGet", championToGet.Image.Base64);
            }
        }

    }
}