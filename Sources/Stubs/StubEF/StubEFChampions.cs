﻿using Microsoft.EntityFrameworkCore;
using EFlib;

namespace StubEF
{
    public class StubEFChampions : SQLiteContext
    {
        /**** Attributs ****/


        /**** Méthodes ****/
        public StubEFChampions(DbContextOptions<SQLiteContext> options)
        : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<EFChampion>().HasData(
                new EFChampion { Name = "Akali", Bio = "maBio1", Icon = "monIcon1" },
                new EFChampion { Name="Aatrox", Bio="maBio2", Icon="monIcon2" },
                new EFChampion { Name = "Ahri", Bio = "maBio3", Icon = "monIcon3" },
                new EFChampion { Name = "Akshan", Bio = "maBio4", Icon = "monIcon4" },
			    new EFChampion { Name = "Bard", Bio = "maBio5", Icon = "monIcon5" },
			    new EFChampion { Name = "Alistar", Bio = "maBio6", Icon = "monIcon6" }
            );
        }
    }
}
