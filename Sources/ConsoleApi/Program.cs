﻿// See https://aka.ms/new-console-template for more information
using API.Dto;
using Newtonsoft.Json;
using System.Net.Http.Headers;

Console.WriteLine("Hello, World!");
Console.WriteLine("Hello, World!");

using (var client = new HttpClient())
{
    var response = await client.GetAsync("https://www.example.com");
    Console.WriteLine(await response.Content.ReadAsStringAsync());
}
static async Task GetJsonData()
{
    using var client = new HttpClient();
    client.BaseAddress = new Uri("https://localhost:7175");
    client.DefaultRequestHeaders.Accept.Clear();
    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

    HttpResponseMessage response = await client.GetAsync("/api/ChampionDto");
    if (response.IsSuccessStatusCode)
    {
        var responseBody = await response.Content.ReadAsStringAsync();
        try
        {
            var champions = JsonConvert.DeserializeObject<List<ChampionDto>>(responseBody);
            if (champions != null)
            {
                foreach (var champion in champions)
                {
                    Console.WriteLine($"Name: {champion.Name}, Bio: {champion.Bio}");
                }
            }
            Console.ReadLine();
        }
        catch (JsonReaderException)
        {
            Console.WriteLine("Something went wrong.");
        }
    }
}