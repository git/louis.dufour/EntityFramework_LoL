# Projet C#
Ce projet a été réalisé durant les cours de Entity framework et Consommation de développement de services. Lors de la 2ème année de BUT à l'IUT Clermont Auvergne. Cette application vous permet de gérer de manipuler une pseudo API de League Of Legends.

## Contributeur(s) 
* [Louis Dufour](https://codefirst.iut.uca.fr/git/louis.dufour)
* [Marc CHEVALDONNE](https://codefirst.iut.uca.fr/git/marc.chevaldonne)

## Ma configuration
* .NET 6 en C#

## Utilisation
* Vous devez tirer la branche 'master'

## Technologie 
Pour ce projet nous utilisons la technologie **Entity Framework Core** qui est un mappeur de base de données objet moderne pour .NET. Il prend en charge les requêtes LINQ, le suivi des modifications, les mises à jour et les migrations de schéma.

La deuxième technologie utilisée est **ASP.NET** qui est une API qui va relier le tout afin de pouvoir de mettre un intermédiaire entre la base de données et le client final.

La notion principale à retenir lors de ce projet, c'est que nous avons appris à utiliser un [**ORM**](https://fr.wikipedia.org/wiki/Mapping_objet-relationnel) c'est-à-dire un object-relational mapping. Un mapping objet-relationnel est un type de programme informatique qui se place en interface entre un programme applicatif et une base de données relationnelle pour simuler une base de données orientée objet.

## Architecture
![Image clique droit](/Documentation/img/Architecture_du_projetV3.png)

## Respect des consignes
### API (*24 points*)
- [X] Mise en place de toutes les opérations CRUD (*4 points*)
- [X] API RESTful (respect des règles de routage, utilisation des bons status code ...) (*2 points*)
- [X] Utilisation des fichiers configurations (*1 points*)
- [X] Versionnage de l'api (avec versionnage de la doc) (*1 point*)
- [X] Logs (*1 point*)
- [X] Tests unitaires (*3 point*)
- [ ] Réalisation du client MAUI et liaison avec l'api (*4 point*)
- [X] Liaison avec la base de données (*2 point*)
- [X] Filtrage + Pagination des données (*1 point*)
- [X] Propreté du code (Vous pouvez vous servir de sonarqube) (*2 point*)
- [X] Dockerisation et Hébergement des API (CodeFirst) (*3 point*)

*Je n'ai pas réussi à totalement remplir les tests unitaires côté API, cela a été le plus dur. C'est-à-dire faire fonctionner les tests lorsque l'api est relié à la base de données. Par contre j'arrive à enregistrer un champion en base de données si vous regardez, mais des erreurs persistent*

*J'ai réussi également à lancer l'application de Marc, mais je n'ai pas eu le temps de la relier à ce que j'ai fait.*

### Documentation (16 points)
- [x] Le Readme (*4 points*) 
- [X] Schéma et description de l'architecture globale de l'application (1 schéma + lien entre partie , min 1 page) (*8 points*)
- [x] Merge request (*2 points*)

## Diagramme de classes du modèle
```mermaid
classDiagram
class LargeImage{
    +/Base64 : string
}
class Champion{
    +/Name : string
    +/Bio : string
    +/Icon : string
    +/Characteristics : Dictionary~string, int~
    ~ AddSkin(skin : Skin) bool
    ~ RemoveSkin(skin: Skin) bool
    + AddSkill(skill: Skill) bool
    + RemoveSkill(skill: Skill) bool
    + AddCharacteristics(someCharacteristics : params Tuple~string, int~[])
    + RemoveCharacteristics(label : string) bool
    + this~label : string~ : int?
}
Champion --> "1" LargeImage : Image
class ChampionClass{
    <<enumeration>>
    Unknown,
    Assassin,
    Fighter,
    Mage,
    Marksman,
    Support,
    Tank,
}
Champion --> "1" ChampionClass : Class
class Skin{
    +/Name : string    
    +/Description : string
    +/Icon : string
    +/Price : float
}
Skin --> "1" LargeImage : Image
Champion "1" -- "*" Skin 
class Skill{
    +/Name : string    
    +/Description : string
}
class SkillType{
    <<enumeration>>
    Unknown,
    Basic,
    Passive,
    Ultimate,
}
Skill --> "1" SkillType : Type
Champion --> "*" Skill
class Rune{
    +/Name : string    
    +/Description : string
}
Rune --> "1" LargeImage : Image
class RuneFamily{
    <<enumeration>>
    Unknown,
    Precision,
    Domination
}
Rune --> "1" RuneFamily : Family
class Category{
    <<enumeration>>
    Major,
    Minor1,
    Minor2,
    Minor3,
    OtherMinor1,
    OtherMinor2
}
class RunePage{
    +/Name : string
    +/this[category : Category] : Rune?
    - CheckRunes(newRuneCategory : Category)
    - CheckFamilies(cat1 : Category, cat2 : Category) bool?
    - UpdateMajorFamily(minor : Category, expectedValue : bool)
}
RunePage --> "*" Rune : Dictionary~Category,Rune~
```

## Diagramme de classes des interfaces de gestion de l'accès aux données
```mermaid
classDiagram
direction LR;
class IGenericDataManager~T~{
    <<interface>>
    GetNbItems() Task~int~
    GetItems(index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~T~~
    GetNbItemsByName(substring : string)
    GetItemsByName(substring : string, index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~T~~
    UpdateItem(oldItem : T, newItem : T) Task~T~~
    AddItem(item : T) Task~T~
    DeleteItem(item : T) Task~bool~
}
class IChampionsManager{
    <<interface>>
    GetNbItemsByCharacteristic(charName : string)
    GetItemsByCharacteristic(charName : string, index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~Champion?~~
    GetNbItemsByClass(championClass : ChampionClass)
    GetItemsByClass(championClass : ChampionClass, index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~Champion?~~
    GetNbItemsBySkill(skill : Skill?)
    GetItemsBySkill(skill : Skill?, index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~Champion?~~
    GetNbItemsBySkill(skill : string)
    GetItemsBySkill(skill : string, index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~Champion?~~
    GetNbItemsByRunePage(runePage : RunePage?)
    GetItemsByRunePage(runePage : RunePage?, index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~Champion?~~
}
class ISkinsManager{
    <<interface>>
    GetNbItemsByChampion(champion : Champion?)
    GetItemsByChampion(champion : Champion?, index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~Skin?~~
}
class IRunesManager{
    <<interface>>
    GetNbItemsByFamily(family : RuneFamily)
    GetItemsByFamily(family : RuneFamily, index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~Rune?~~
}
class IRunePagesManager{
    <<interface>>
    GetNbItemsByRune(rune : Rune?)
    GetItemsByRune(rune : Rune?, index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~RunePage?~~
    GetNbItemsByChampion(champion : Champion?)
    GetItemsByChampion(champion : Champion?, index : int, count : int, orderingPropertyName : string?, descending : bool) Task~IEnumerable~RunePage?~~
}

IGenericDataManager~Champion?~ <|.. IChampionsManager : T--Champion?
IGenericDataManager~Skin?~ <|.. ISkinsManager : T--Skin?
IGenericDataManager~Rune?~ <|.. IRunesManager : T--Rune?
IGenericDataManager~RunePage?~ <|.. IRunePagesManager : T--RunePage?
class IDataManager{
    <<interface>>
}
IChampionsManager <-- IDataManager : ChampionsMgr
ISkinsManager <-- IDataManager : SkinsMgr
IRunesManager <-- IDataManager : RunesMgr
IRunePagesManager <-- IDataManager : RunePagesMgr
```

## Diagramme de classes simplifié du Stub
```mermaid
classDiagram
direction TB;

IDataManager <|.. StubData

ChampionsManager ..|> IChampionsManager
StubData --> ChampionsManager

RunesManager ..|> IRunesManager
StubData --> RunesManager

RunePagesManager ..|> IRunePagesManager
StubData --> RunePagesManager

SkinsManager ..|> ISkinsManager
StubData --> SkinsManager

StubData --> RunesManager
StubData --> "*" Champion
StubData --> "*" Rune
StubData --> "*" RunePages
StubData --> "*" Skins
```
